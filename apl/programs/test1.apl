integer main()
{
	integer childpid, retval,f,a,b,c;
	a = Fork();
	if(a == -2) then
		retval = Exec("sme.xsm");
	endif;
	b = Fork();
	if(b == -2) then
		retval = Exec("sme.xsm");
	endif;		
	c = Fork();
	if(c == -2) then
		retval = Exec("sme.xsm");
	endif;
	
	f = Wait(a);
	f = Wait(b);
	f = Wait(c);

	f = Signal();

	f = Wait(a);
	f = Wait(b);
	f = Wait(c);

	Exit();
        
	
	
	
	return 0;
}
