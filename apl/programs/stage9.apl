integer main()
{
	integer pid,f,g;
	string x;
	print("Before Fork");
	f = Create("aabc");
	print(f);
	f = Open("aabc");
	pid = Fork();
	print(pid);
	if(pid == -2) then
		print("trying close");
		g=Close(7);
		print(g);
	endif;
	print("After Fork");
	return 0;
}
