decl
	integer checkPrime(integer n);
enddecl

integer checkPrime(integer n)
{
	integer i, r;
	i = 2;
	r = -1;
	if n == 1 then
		r = 1;
	endif;
	if n == 2 then
		r = 2;
	endif;
	while i < n do
		if r != -1 then
			break;
		endif;
		if (n % i == 0) then
			r = n;
		endif;
		i = i + 1;
	endwhile;
	return r;
}

integer main()
{
	print("Input n: ");
	integer n;
	read(n);
	integer i;
	i = 1;
	while i < n do
		if checkPrime(i) != 0 then
			print(checkPrime(i));
		endif;
	endwhile;
	return 0;
}
