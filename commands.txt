spl int=1 int1_8.spl
spl int=2 int2_8.spl
spl int=3 int3_8.spl
spl int=4 int4_8.spl
spl int=5 int5_11.spl
spl int=6 int6_11.spl
spl int=7 int7_11.spl
spl int=timer Timer.spl
spl os os_startup_11.spl

apl stage11.apl

fdisk
load --int=1 ../spl/programs/int1.xsm
load --int=2 ../spl/programs/int2.xsm
load --int=3 ../spl/programs/int3.xsm
load --int=4 ../spl/programs/int4.xsm
load --int=5 ../spl/programs/int5.xsm
load --int=6 ../spl/programs/int6.xsm
load --int=7 ../spl/programs/int7.xsm
load --int=timer ../spl/programs/timer.xsm
load --exhandler ../spl/programs/exhandler.xsm
load --os ../spl/programs/os_startup.xsm
load --init ../apl/programs/stage10.xsm
exit

